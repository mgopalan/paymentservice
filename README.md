## Current Scenario

 

Customers are Authorized for the full amount of their Order at the time of placing the Order.
When the first Consignment is picked and packed, Carrefour Captures the full amount that was Authorized. If some items were out of stock or unavailable during the picking of this first Consignment, we deduct the amount before Capturing the amount. 
This means that we aggressively Capture money for consignments that may not have been picked yet (in multi-consignment use-case). In case, these consignments also have out of stock or unavailable items (or canceled before picking), we trigger refunds equal to the amount of the items that are not fulfilled.
In case, the first consignment does not get picked or packed for more than 3 days, we Capture the full amount of the order. This is due to the fact that the Authorization for the amount expires after 3 days, rendering it risky to attempt to Capture the amount at a later date.

 

## Problems
### Customer (Mis)Communication: 
1)    When we Authorize a transaction, some banks send a communication to the customer that Carrefour has charged them a certain amount. This leads the customer to believe that the money has left his/her account into Carrefour’s coffers. 

2)    When we Capture amount, some banks send another communication to the customer that Carrefour has charged them a certain amount. This leads the customer to believe that Carrefour has charged them twice. 

3)    When we refund a certain amount to the customer, the transaction can take up to 10 business days to be remitted to their account. Some banks do not inform them about the refund. This leads the customer to believe that the refund never happened. 

### Incorrect Behavior:

1)    In times such as Covid-19, our operations are strained and are struggling to keep up with demand. When we Capture the full amount of the order after 3 days WITHOUT pick and pack activity, we are transferring customer’s money without clear visibility on when we will provide the corresponding goods. This leads to customer losing the money and still not having Milk and Diapers at home.

2)    We Capture money via a cron instead of Event driven. This means that two instances of the Cron tends to Capture money twice from the customer.



## Solution

1)    Move to Event Driven instead of Cron to prevent double charging. Events are Customer clicks Place Order, consignment is picked, Customer cancels consignment, Customer returns consignment.

2)    For low risk orders i.e. Order Value < AED 500 (typically food orders), DO NOT Authorize for full amount. Instead validate that the Payment Instrument (Card) is not expired, valid etc. This will prevent customers from getting messages at the time of order placement for (typically) food orders.

3)    For high risk orders i.e. Order Value > AED 500 (typically electronics included), Charge the entire amount when the customer places the order. The message that the bank sends out will actually mean what we did in this case reducing customer confusion.

4)    For any and all refunds that we process, we immediately send a message to the customer reflecting the Order, the amount refunded and the date of the refund. This will ensure that the customer’s trust is gained.